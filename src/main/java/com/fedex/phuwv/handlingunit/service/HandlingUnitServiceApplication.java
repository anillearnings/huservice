package com.fedex.phuwv.handlingunit.service;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.actuate.autoconfigure.security.servlet.ManagementWebSecurityAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.context.annotation.ComponentScan;

import com.fedex.phuwv.view.handlingunitentity.entity.HandlingUnitEntity;

@SpringBootApplication(exclude= {SecurityAutoConfiguration.class,ManagementWebSecurityAutoConfiguration.class})
@EnableDiscoveryClient
@EnableCircuitBreaker
@EnableJpaRepositories
@EntityScan(basePackageClasses = { HandlingUnitEntity.class })
@ComponentScan(basePackages = "com.fedex.phuwv")
public class HandlingUnitServiceApplication {
	public static void main(String[] args) {
		SpringApplication.run(HandlingUnitServiceApplication.class, args);
	}
}