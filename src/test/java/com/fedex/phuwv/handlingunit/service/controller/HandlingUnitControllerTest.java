//package com.fedex.phuwv.handlingunit.service.controller;
//
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
//
//import java.util.ArrayList;
//
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.Test;
//import org.junit.jupiter.api.extension.ExtendWith;
//import org.mockito.InjectMocks;
//import org.mockito.Mock;
//import org.mockito.Mockito;
//import org.springframework.http.MediaType;
//import org.springframework.test.context.junit.jupiter.SpringExtension;
//import org.springframework.test.web.servlet.MockMvc;
//import org.springframework.test.web.servlet.setup.MockMvcBuilders;
//
//import com.fedex.phuwv.common.dto.HandlingUnit;
//import com.fedex.phuwv.handlingunit.service.service.HandlingUnitService;
//
//@ExtendWith(SpringExtension.class)
//public class HandlingUnitControllerTest {
//
//	private MockMvc mockMvc;
//
//	@InjectMocks
//	private HandlingUnitController handlingUnitController;
//
//	@Mock
//	private HandlingUnitService handlingUnitService;
//
//	@BeforeEach
//	public void setUp() {
//		mockMvc = MockMvcBuilders.standaloneSetup(handlingUnitController).setControllerAdvice().build();
//	}
//
//	@Test
//	public void givenHandlingUnitUUIDWhenGetLatestHandlingUnitByUUIDThenReturn200StatusHandlingModel()
//			throws Exception {
//		Mockito.when(handlingUnitService.getLatestHandlingUnitByUUID(Mockito.anyString()))
//				.thenReturn(new HandlingUnit());
//
//		mockMvc.perform(get("/api/v1/phuwv/handlingUnits/handlingUnitUUID").accept(MediaType.APPLICATION_JSON_VALUE))
//				.andExpect(status().isOk());
//	}
//
//	@Test
//	public void givenShipmentUUIDWhenGetLatestHandlingUnitByShipmentUUIDThenReturn200StatusAndHandlingUnitModel()
//			throws Exception {
//		Mockito.when(handlingUnitService.getLatestHandlingUnitByShipmentUUID(Mockito.anyString()))
//				.thenReturn(new HandlingUnit());
//		mockMvc.perform(get("/api/v1/phuwv/handlingUnits/getLatestHandlingUnit/shipmentUUID")
//				.accept(MediaType.APPLICATION_JSON_VALUE)).andExpect(status().isOk());
//	}
//
//	@Test
//	public void givenShipmentUUIDWheGgetListOfLatestHandlingUnitByShipmentUUIDThenReturn200StatusandListOfHandlingUnitModel()
//			throws Exception {
//		Mockito.when(handlingUnitService.getListOfLatestHandlingUnitByShipmentUUID(Mockito.anyString()))
//				.thenReturn(new ArrayList<HandlingUnit>());
//		mockMvc.perform(
//				get("/api/v1/phuwv/handlingUnits/byShipmentUUID/shipmentUUID").accept(MediaType.APPLICATION_JSON_VALUE))
//				.andExpect(status().isOk());
//	}
//
//	@Test
//	public void givenTrackingNbrWhenGetLatestHandlingUnitByTrackNbrThenReturn200StatusAndHandlingUnitModel()
//			throws Exception {
//		Mockito.when(handlingUnitService.getLatestHandlingUnitByTrackNbr(Mockito.anyString()))
//				.thenReturn(new com.fedex.phuwv.common.dto.HandlingUnit());
//		mockMvc.perform(get("/api/v1/phuwv/handlingUnits?trkNbr=12345").accept(MediaType.APPLICATION_JSON_VALUE))
//				.andExpect(status().isOk());
//	}
//
//	@Test
//	public void givenPurgeUntilDateWhenPurgeUntilDateThenReturn200StatusAndCount() throws Exception {
//		Mockito.when(handlingUnitService.purge(Mockito.anyString())).thenReturn(Integer.valueOf(0));
//		mockMvc.perform(delete("/api/v1/phuwv/purgeUntil/purgeDt").accept(MediaType.APPLICATION_JSON_VALUE))
//				.andExpect(status().isOk());
//	}
//}