package com.fedex.phuwv.handlingunit.service.util;

public class HandlingUnitServiceConstants {
    public static final String PURGEDTPATTERN="yyyy-MM-dd hh:mm:ss";
    public static final String ADDITIONAL_ROWS="Additional Rows";
    public static final String NO_HU_BATCHES="Number of batches to be purged";
    public static final String PLUS="plus";
    public static final String PURGED_MSG ="-DIVE- Purged ";
    public static final String RETRIEVED_MSG ="-DIVE- Retrieved ";
    public static final String CREATING_JSON ="Creating json";
    public static final String EXCEPTION_MSG ="Caught Exception trying to delete handling unit Batch ";
    public static final String HANDLINGUNIT_NOT_FOUND= "No handlingUnit found by uuid:";
    
    
}
