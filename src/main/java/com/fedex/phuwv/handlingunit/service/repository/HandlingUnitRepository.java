package com.fedex.phuwv.handlingunit.service.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.fedex.phuwv.view.handlingunitentity.entity.HandlingUnitEntity;

@Repository
@Transactional

public interface HandlingUnitRepository extends JpaRepository<HandlingUnitEntity, String> {

	public List<HandlingUnitEntity> findByHandlingUnitId_handlingUnitUUIDOrderByEventCreateTimestampDesc(
			String handlingUnitUUID);

	public List<HandlingUnitEntity> findFirst50000ByRowPurgeDtLessThanEqual(Date purgeDate);

	public List<HandlingUnitEntity> findFirst50000ByRowPurgeDtIsNull();

	public List<HandlingUnitEntity> findByShipmentUUIDOrderByEventCreateTimestampDesc(String shipmentUUID);

	public List<HandlingUnitEntity> findByHandlingUnitId_handlingUnitUUID(String handlingUnitUUID);

}
