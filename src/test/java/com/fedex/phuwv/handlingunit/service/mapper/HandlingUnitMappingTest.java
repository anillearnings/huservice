//package com.fedex.phuwv.handlingunit.service.mapper;
//
//import static org.assertj.core.api.Assertions.assertThat;
//
//import java.io.IOException;
//import java.nio.charset.Charset;
//import java.nio.file.Files;
//import java.nio.file.Paths;
//
//import org.junit.jupiter.api.Test;
//import org.mapstruct.factory.Mappers;
//import org.mockito.Spy;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.boot.test.mock.mockito.MockBean;
//import org.springframework.test.context.ActiveProfiles;
//import org.springframework.test.web.servlet.MockMvc;
//
//import com.fasterxml.jackson.databind.ObjectMapper;
//import com.fedex.phuwv.common.dto.HandlingUnit;
//import com.fedex.phuwv.common.mapper.HandlingUnitModelMapper;
//import com.fedex.phuwv.handlingunit.service.service.HandlingUnitService;
//import com.fedex.sefs.core.handlingunit.v1.api.HandlingUnitDomainEvent;
//
//import lombok.extern.slf4j.Slf4j;
//
//@SpringBootTest
//@AutoConfigureMockMvc
//@ActiveProfiles("L0")
//@Slf4j
//public class HandlingUnitMappingTest {
//
//	@Autowired
//	MockMvc mockMvc;
//
//	@MockBean
//	HandlingUnitService service;
//
//	@Autowired
//	ObjectMapper defaultObjectMapper;
//
//	@Spy
//	private HandlingUnitModelMapper modelMapstructMapper = Mappers.getMapper(HandlingUnitModelMapper.class);
//
//	@Test
//	public void checkHandlingUnitUUID() throws IOException {
//
//		String content = readFile("src/test/resources/HandlingUnit Domain Event Sample.json", Charset.defaultCharset());
//		HandlingUnitDomainEvent coreHU = defaultObjectMapper.readValue(content, HandlingUnitDomainEvent.class);
//
//		try {
//			HandlingUnit model = modelMapstructMapper.toModel(coreHU);
//			System.out.println("handlingUnit model: " + model.toString());
//			assertThat(model.getHuUUID()).isEqualTo("521d02f3-9a0b-3ffb-9fa5-2a591e245114");
//		} catch (Exception e) {
//			log.info("exception while check HandlingUnit UUID");
//		}
//	}
//
//	static String readFile(String path, Charset encoding) throws IOException {
//		byte[] encoded = Files.readAllBytes(Paths.get(path));
//		return new String(encoded, encoding);
//	}
//}
