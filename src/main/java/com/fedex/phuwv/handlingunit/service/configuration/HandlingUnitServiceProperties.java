package com.fedex.phuwv.handlingunit.service.configuration;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.ConfigurationProperties;

import javax.annotation.PostConstruct;

@ConfigurationProperties(prefix = "phuwv.handlingunit")
@Slf4j
@Getter
@Setter
@NoArgsConstructor
public class HandlingUnitServiceProperties {

    private int huBatchSize;
    private int huXrefBatchSize;
    private boolean huPurgeNull;
    private boolean huPurgeDaily;

    @PostConstruct
    public void initialize() {
    	log.info(" huBatchSize Property initialized" +huBatchSize);
    	log.info(" huXrefBatchSize Property initialized" +huXrefBatchSize);
    	log.info(" huPurgeNull Property initialized" +huPurgeNull);
    	log.info(" huPurgeDaily Property initialized" +huPurgeDaily);
        log.info("Properties initialized");
    }

}
