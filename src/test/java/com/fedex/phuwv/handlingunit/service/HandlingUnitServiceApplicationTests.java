//package com.fedex.phuwv.handlingunit.service;
//import static org.assertj.core.api.Assertions.assertThat;
//
//import org.junit.jupiter.api.Test;
//import org.junit.jupiter.api.extension.ExtendWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.test.context.junit.jupiter.SpringExtension;
//
//import com.fedex.phuwv.handlingunit.service.configuration.HandlingUnitServiceProperties;
// 
//@ExtendWith(SpringExtension.class)
//@SpringBootTest
//public class HandlingUnitServiceApplicationTests {
//	@Autowired
//	HandlingUnitServiceProperties properties;
// 
//    @Test
//    public void contextLoads() {
//    	properties.initialize();
//		properties.setHuBatchSize(10);
//		properties.setHuPurgeDaily(true);
//		properties.setHuPurgeNull(true);
//		assertThat(properties.getHuBatchSize()).isEqualTo(10);
//    }
//    
//}