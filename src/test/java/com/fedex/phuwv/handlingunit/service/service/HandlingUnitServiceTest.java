//package com.fedex.phuwv.handlingunit.service.service;
//
//import static org.assertj.core.api.Assertions.assertThat;
//import static org.junit.Assert.assertNotEquals;
//import static org.junit.jupiter.api.Assertions.assertEquals;
//import static org.mockito.Mockito.when;
//
//import java.io.IOException;
//import java.nio.charset.Charset;
//import java.nio.file.Files;
//import java.nio.file.Paths;
//import java.text.ParseException;
//import java.time.Instant;
//import java.util.ArrayList;
//import java.util.Date;
//import java.util.List;
//
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.Test;
//import org.junit.jupiter.api.extension.ExtendWith;
//import org.mapstruct.factory.Mappers;
//import org.mockito.InjectMocks;
//import org.mockito.Mock;
//import org.mockito.Mockito;
//import org.mockito.Spy;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.test.context.ActiveProfiles;
//import org.springframework.test.context.junit.jupiter.SpringExtension;
//
//import com.fasterxml.jackson.databind.ObjectMapper;
//import com.fedex.phuwv.common.dto.HandlingUnit;
//import com.fedex.phuwv.common.mapper.HandlingUnitModelMapper;
//import com.fedex.phuwv.handlingunit.service.configuration.HandlingUnitServiceProperties;
//import com.fedex.phuwv.handlingunit.service.repository.HandlingUnitRepository;
//import com.fedex.phuwv.handlingunit.service.repository.HandlingUnitRepositoryDatabaseTest;
//import com.fedex.phuwv.handlingunit.service.repository.HandlingUnitXRefRepository;
//import com.fedex.phuwv.view.handlingunitentity.entity.HandlingUnitEntity;
//import com.fedex.phuwv.view.handlingunitentity.entity.HandlingUnitId;
//import com.fedex.phuwv.view.handlingunitentity.entity.HandlingUnitXRefEntity;
//import com.fedex.phuwv.view.handlingunitentity.entity.HandlingUnitXRefId;
//import com.fedex.sefs.core.handlingunit.v1.api.HandlingUnitDomainEvent;
//
//@ExtendWith(SpringExtension.class)
//@ActiveProfiles("L0")
//@SpringBootTest
//@AutoConfigureMockMvc
//public class HandlingUnitServiceTest {
//
//	@InjectMocks
//	private HandlingUnitService serviceMock;
//
//	@Mock
//	private HandlingUnitRepository handlingUnitRepository;
//
//	@Mock
//	HandlingUnitXRefRepository handlingUnitXRefRepository;
//	
//	@Mock
//	HandlingUnitServiceProperties handlingUnitServiceProperties;
//
//	@Autowired
//	private ObjectMapper defaultObjectMapper;
//	private static final String path = "src/test/resources/hu_sample2.json";
//
//	@Spy
//	private HandlingUnitModelMapper modelMapstructMapper = Mappers.getMapper(HandlingUnitModelMapper.class);
//
//	
//	private HandlingUnitId handlingUnitId;
//	private List<HandlingUnitEntity> handlingUnitList;
//	private List<HandlingUnitXRefEntity> entityList;
//	private static String huUUID = "521d02f3-9a0b-3ffb-9fa5-2a591e245114";
//	private String trackingNr = "597783866512";
//
//	final Logger logger = LoggerFactory.getLogger(HandlingUnitRepositoryDatabaseTest.class);
//
//	@BeforeEach
//	public void setup() throws IOException {
//
//		String content = readFile("src/test/resources/hu_sample2.json", Charset.defaultCharset());
//		HandlingUnitDomainEvent coreHandlingUnit = defaultObjectMapper.readValue(content,
//				HandlingUnitDomainEvent.class);
//
//		HandlingUnitEntity handlingUnitEntity = new HandlingUnitEntity();
//		handlingUnitId = new HandlingUnitId(huUUID, huUUID, Instant.now());
//		handlingUnitEntity.setHandlingUnitId(handlingUnitId);
//		handlingUnitEntity.setEventType("EventType");
//		handlingUnitEntity.setEventCreateTimestamp(null);
//		handlingUnitList = new ArrayList<HandlingUnitEntity>();
//		handlingUnitList.add(handlingUnitEntity);
//
//	}
//
//	@Test
//	public void testGetLatestHandlingUnitByUUID() throws IOException {
//		String content = readFile(path, Charset.defaultCharset());
//
//		HandlingUnitDomainEvent coreHU = defaultObjectMapper.readValue(content, HandlingUnitDomainEvent.class);
//
//		try {
//			HandlingUnit model = modelMapstructMapper.toModel(coreHU);
//
//			when(handlingUnitRepository.findByHandlingUnitId_handlingUnitUUID(huUUID)).thenReturn(handlingUnitList);
//			when(serviceMock.getLatestHandlingUnitByUUID(huUUID)).thenReturn(model);
//			assertEquals(serviceMock.getLatestHandlingUnitByUUID(huUUID), model);
//		} catch (Exception e) {
//			logger.error("testGetLatestHandlingUnitByUUID failed" + e);
//		}
//	}
//
//	@Test
//	public void testGetLatestHandlingUnitByUUIDFailure() throws IOException {
//		String content = readFile(path, Charset.defaultCharset());
//
//		HandlingUnitDomainEvent coreHU = defaultObjectMapper.readValue(content, HandlingUnitDomainEvent.class);
//		List<HandlingUnitEntity> handlingUnitEntity = new ArrayList<HandlingUnitEntity>();
//
//		try {
//			HandlingUnit model = modelMapstructMapper.toModel(coreHU);
//
//			when(handlingUnitRepository.findByHandlingUnitId_handlingUnitUUID(huUUID)).thenReturn(handlingUnitEntity);
//			when(serviceMock.getLatestHandlingUnitByUUID(huUUID)).thenReturn(model);
//			assertEquals(serviceMock.getLatestHandlingUnitByUUID(huUUID), model);
//		} catch (Exception e) {
//			logger.error("testGetLatestHandlingUnitByUUID_failure failed" + e);
//		}
//	}
//
//	@Test
//	public void testListOfLatestHandlingUnitByShipmentUUID() throws IOException {
//		String content = readFile("src/test/resources/hu_sample_list.json", Charset.defaultCharset());
//
//		HandlingUnitDomainEvent[] coreHU = defaultObjectMapper.readValue(content, HandlingUnitDomainEvent[].class);
//
//		List<HandlingUnit> model = new ArrayList<HandlingUnit>();
//
//		for (HandlingUnitDomainEvent item : coreHU) {
//
//			model.add(modelMapstructMapper.toModel(item));
//		}
//		try {
//
//			when(handlingUnitRepository.findByShipmentUUIDOrderByEventCreateTimestampDesc(huUUID))
//					.thenReturn(handlingUnitList);
//			when(serviceMock.getListOfLatestHandlingUnitByShipmentUUID(huUUID)).thenReturn(model);
//
//			assertThat(serviceMock.getListOfLatestHandlingUnitByShipmentUUID(huUUID)).isEqualTo(model);
//		} catch (Exception e) {
//			logger.error("testListOfLatestHandlingUnitByShipmentUUID failed" + e);
//		}
//	}
//
//	@Test
//	public void testListOfLatestHandlingUnitByShipmentUUIDFailure() throws IOException {
//		String content = readFile("src/test/resources/hu_sample_list.json", Charset.defaultCharset());
//
//		HandlingUnitDomainEvent[] coreHU = defaultObjectMapper.readValue(content, HandlingUnitDomainEvent[].class);
//
//		List<HandlingUnit> model = new ArrayList<HandlingUnit>();
//
//		for (HandlingUnitDomainEvent item : coreHU) {
//
//			model.add(modelMapstructMapper.toModel(item));
//		}
//		try {
//			List<HandlingUnitEntity> handlingUnitEntity = new ArrayList<HandlingUnitEntity>();
//			when(handlingUnitRepository.findByShipmentUUIDOrderByEventCreateTimestampDesc(huUUID))
//					.thenReturn(handlingUnitEntity);
//			assertNotEquals(null, serviceMock.getListOfLatestHandlingUnitByShipmentUUID(huUUID));
//		} catch (Exception e) {
//			logger.error("testListOfLatestHandlingUnitByShipmentUUID_failure failed" + e);
//		}
//	}
//
//	@Test
//	public void testGetLatestHandlingUnitByShipmentUUID() throws IOException {
//		String content = readFile(path, Charset.defaultCharset());
//
//		HandlingUnitDomainEvent coreHU = defaultObjectMapper.readValue(content, HandlingUnitDomainEvent.class);
//
//		try {
//			HandlingUnit model = modelMapstructMapper.toModel(coreHU);
//			when(handlingUnitRepository.findByShipmentUUIDOrderByEventCreateTimestampDesc(huUUID))
//					.thenReturn(handlingUnitList);
//			when(serviceMock.getLatestHandlingUnitByShipmentUUID(huUUID)).thenReturn(model);
//
//			assertThat(serviceMock.getLatestHandlingUnitByShipmentUUID(huUUID)).isEqualTo(model);
//		} catch (Exception e) {
//			logger.error("testGetLatestHandlingUnitByShipmentUUID failed" + e);
//		}
//	}
//
//	@Test
//	public void getPurgeRowsForHURefTest() throws IOException {
//
//		try {
//			List<HandlingUnitXRefEntity> batchList = new ArrayList<HandlingUnitXRefEntity>();
//			handlingUnitXRefRepository.deleteInBatch(batchList);
//			when(serviceMock.getPurgeRowsForHURef(Mockito.anyInt(), Mockito.anyList(), Mockito.anyInt(),
//					Mockito.anyInt())).thenReturn(1);
//
//		} catch (Exception e) {
//			logger.error("getPurgeRowsForHURefTest failed" + e);
//		}
//	}
//
//	@Test
//	public void testGetLatestHandlingUnitByShipmentUUIDFailure() throws IOException {
//		String content = readFile(path, Charset.defaultCharset());
//
//		HandlingUnitDomainEvent coreHU = defaultObjectMapper.readValue(content, HandlingUnitDomainEvent.class);
//		List<HandlingUnitEntity> handlingUnitEntity = new ArrayList<HandlingUnitEntity>();
//		try {
//
//			HandlingUnit model = modelMapstructMapper.toModel(coreHU);
//			when(handlingUnitRepository.findByShipmentUUIDOrderByEventCreateTimestampDesc(huUUID))
//					.thenReturn(handlingUnitEntity);
//			when(serviceMock.getLatestHandlingUnitByShipmentUUID(huUUID)).thenReturn(model);
//
//			assertThat(serviceMock.getLatestHandlingUnitByShipmentUUID(huUUID)).isEqualTo(model);
//		} catch (Exception e) {
//			logger.error("testGetLatestHandlingUnitByUUID failed" + e);
//		}
//	}
//
//	@Test
//	public void testGetHandlingUnitByTrkNbr() throws IOException {
//		String content = readFile(path, Charset.defaultCharset());
//		HandlingUnitDomainEvent coreHU = defaultObjectMapper.readValue(content, HandlingUnitDomainEvent.class);
//
//		try {
//			HandlingUnitXRefEntity handlingUnitXRefEntity = new HandlingUnitXRefEntity();
//			HandlingUnitXRefId handlingUnitXRefId = new HandlingUnitXRefId("handlingUnitUUID", "trackingNbr",
//					"trackingNbrType");
//			handlingUnitXRefEntity.setHandlingUnitRefId(handlingUnitXRefId);
//			handlingUnitXRefEntity.setRowPurgeData(null);
//			entityList = new ArrayList<HandlingUnitXRefEntity>();
//			entityList.add(handlingUnitXRefEntity);
//			HandlingUnit model = modelMapstructMapper.toModel(coreHU);
//			when(handlingUnitXRefRepository.findTopByHandlingUnitRefId_trackingNbr(trackingNr)).thenReturn(entityList.get(0));
//			when(serviceMock.getLatestHandlingUnitByTrackNbr(trackingNr)).thenReturn(model);
//			assertThat(serviceMock.getHUModel(handlingUnitList)).isEqualTo(model);
//			assertThat(serviceMock.getLatestHandlingUnitByUUID(trackingNr)).isEqualTo(model);
//		} catch (Exception e) {
//			logger.error("testGetHandlingUnitByTrkNbr failed" + e);
//		}
//
//	}
//
//	@Test
//	public void testGetHandlingUnitByTrkNbrFailure() throws IOException {
//		String content = readFile(path, Charset.defaultCharset());
//		HandlingUnitDomainEvent coreHU = defaultObjectMapper.readValue(content, HandlingUnitDomainEvent.class);
//
//		try {
//			ArrayList<HandlingUnitXRefEntity> entityListEmpty = new ArrayList<HandlingUnitXRefEntity>();
//			HandlingUnit model = modelMapstructMapper.toModel(coreHU);
//			when(handlingUnitXRefRepository.findTopByHandlingUnitRefId_trackingNbr(trackingNr))
//					.thenReturn(entityListEmpty.get(0));
//			when(serviceMock.getLatestHandlingUnitByTrackNbr(trackingNr)).thenReturn(null);
//			assertNotEquals(model, serviceMock.getLatestHandlingUnitByUUID(trackingNr));
//		} catch (Exception e) {
//			logger.error("testGetHandlingUnitByTrkNbr_failure failed" + e);
//		}
//
//	}
//	@Test
//	void testPurgeSuccess() throws ParseException {
//		Date d1 = new Date();
//		
//		when( handlingUnitRepository
//				.findFirst50000ByRowPurgeDtLessThanEqual(d1)).thenReturn(createHandlingUnitEntity());
//		when( handlingUnitRepository
//				.findFirst50000ByRowPurgeDtIsNull()).thenReturn(createHandlingUnitEntity());
//		when(handlingUnitXRefRepository
//				.findFirst50000ByRowPurgeDataLessThanEqual(d1)).thenReturn(createHandlingUnitXRefEntity());
//		when(handlingUnitXRefRepository.findFirst50000ByRowPurgeDataIsNull()).thenReturn(createHandlingUnitXRefEntity());
//		when( handlingUnitServiceProperties.getHuBatchSize()).thenReturn(1);
//		when(handlingUnitServiceProperties.getHuXrefBatchSize()).thenReturn(1);
//		when(handlingUnitServiceProperties.isHuPurgeDaily()).thenReturn(true);
//		when(handlingUnitServiceProperties.isHuPurgeNull()).thenReturn(true);
//	
//		
//		serviceMock.purge("0000-00-00");
//
//		assertThat(createHandlingUnitEntity()).isNotNull();
//
//	}
//	@Test
//	void testPurgefailure() throws ParseException {
//		Date d1 = new Date();
//		when(handlingUnitServiceProperties.isHuPurgeDaily()).thenReturn(true);
//		when(handlingUnitServiceProperties.isHuPurgeNull()).thenReturn(true);
//		when( handlingUnitServiceProperties.getHuBatchSize()).thenReturn(500);
//		when(handlingUnitServiceProperties.getHuXrefBatchSize()).thenReturn(500);
//		when( handlingUnitRepository
//				.findFirst50000ByRowPurgeDtLessThanEqual(d1)).thenReturn(createHandlingUnitEntity());
//		when(handlingUnitXRefRepository
//				.findFirst50000ByRowPurgeDataLessThanEqual(d1)).thenReturn(createHandlingUnitXRefEntity());
//		when(handlingUnitXRefRepository.findFirst50000ByRowPurgeDataIsNull()).thenReturn(createHandlingUnitXRefEntity());
//	
//		serviceMock.purge("0000-00-00");
//
//		assertThat(createHandlingUnitEntity()).isNotNull();
//		
//	}
//
//	static String readFile(String path, Charset encoding) throws IOException {
//		byte[] encoded = Files.readAllBytes(Paths.get(path));
//		return new String(encoded, encoding);
//	}
//	public List<HandlingUnitEntity> createHandlingUnitEntity() {
//
//	
//		List<HandlingUnitEntity> hulist = new ArrayList<>(); 
//		try {
//		String contents = readFile("src/test/resources/hu_sample2.json", Charset.defaultCharset());
//		HandlingUnitDomainEvent coreHandlingUnit = defaultObjectMapper.readValue(contents,
//				HandlingUnitDomainEvent.class);
//		}catch (Exception e) {
//			logger.error("createHandlingUnitEntity failed" + e);
//		}
//		HandlingUnitEntity huEntity = new HandlingUnitEntity();
//		HandlingUnitId huId = new HandlingUnitId(huUUID, huUUID, Instant.now());;
//		huEntity.setHandlingUnitId(huId);
//		huEntity.setEventType("EventType");
//		huEntity.setEventCreateTimestamp(null);
//		hulist.add(huEntity);
//		return hulist;
//	}
//	public List<HandlingUnitXRefEntity> createHandlingUnitXRefEntity() {
//		List<HandlingUnitXRefEntity> XRef = new ArrayList<>();
//		HandlingUnitXRefEntity handlingUnitXRef = new HandlingUnitXRefEntity();
//		HandlingUnitXRefId XRefId = new HandlingUnitXRefId("handlingUnitUUID", "trackingNbr",
//				"trackingNbrType");
//		handlingUnitXRef.setHandlingUnitRefId(XRefId);
//		handlingUnitXRef.setRowPurgeData(null);
//		XRef.add(handlingUnitXRef);
//		return XRef;
//	}
//
//}
