package com.fedex.phuwv.common.mapper;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.mapstruct.BeforeMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.Mappings;

import com.fedex.phuwv.common.dto.CompositeNaturalId;
import com.fedex.phuwv.common.dto.Dimensions;
import com.fedex.phuwv.common.dto.HandlingUnitId;
import com.fedex.phuwv.common.dto.HandlingUnitPhysicalCharacteristic;
import com.fedex.phuwv.common.dto.LinearUnits;
import com.fedex.phuwv.common.dto.Money;
import com.fedex.phuwv.common.dto.Parameter;
import com.fedex.phuwv.common.dto.ShipmentCommodityDetail;
import com.fedex.phuwv.common.dto.ShipmentDetails;
import com.fedex.phuwv.common.dto.Volume;
import com.fedex.phuwv.common.dto.VolumeUnits;
import com.fedex.phuwv.common.dto.Weight;
import com.fedex.phuwv.common.dto.WeightUnits;
import com.fedex.phuwv.common.dto.WorkAttribute;
import com.fedex.phuwv.common.dto.WorkRequirement;
import com.fedex.phuwv.common.dto.WorkRequirementAssociation;
import com.fedex.phuwv.common.dto.WorkRequirementStatus;
import com.fedex.phuwv.common.dto.WorkSource;
import com.fedex.sefs.core.handlingunit.v1.api.DescriptionList;
import com.fedex.sefs.core.handlingunit.v1.api.HandlingUnit;
import com.fedex.sefs.core.handlingunit.v1.api.HandlingUnitDomainEvent;

@Mapper(componentModel = "spring")
public interface HandlingUnitModelMapper {

	@BeforeMapping
	default void beforeMapping(@MappingTarget com.fedex.phuwv.common.dto.HandlingUnit model,
			HandlingUnitDomainEvent event) {

		try {

			if (event != null) {
				if (event.getAssociations() != null && !(event.getAssociations().isEmpty())) {
					ShipmentDetails shipmentDetails = new ShipmentDetails();
					shipmentDetails.setShipmentUUID(event.getAssociations().stream()
							.filter(association -> association.getAssociationLevel().equalsIgnoreCase("SHIPMENT"))
							.findAny().get().getAssociationUUID());
					model.setShipment(shipmentDetails);
				}
				// model.setPhuwvReleaseVersion(event.getSdomReleaseVersion());
				model.setPhuwvReleaseVersion("2.1.1");
				HandlingUnit huEvent = event.getHandlingUnit();

				if (null != huEvent) {
					if (huEvent.getSystemIdentificationProfile() != null) {
						model.setHuUUID(String.valueOf(huEvent.getSystemIdentificationProfile().getUuid()));

					}

					if (huEvent.getHandlingUnitDetail() != null) {

						if (huEvent.getHandlingUnitDetail().getFedExOperationalFormId() != null) {
							model.setFormID(huEvent.getHandlingUnitDetail().getFedExOperationalFormId());
						}
						if (huEvent.getHandlingUnitDetail().getOwningOpCo() != null) {
							model.setOwningOpCo(huEvent.getHandlingUnitDetail().getOwningOpCo());
						}

						if (huEvent.getHandlingUnitDetail().getPackagingDescription() != null) {
							model.setLegacyPackageCode(
									huEvent.getHandlingUnitDetail().getPackagingDescription().getCode());
						}

						if (huEvent.getHandlingUnitDetail().getType() != null) {
							model.setType(huEvent.getHandlingUnitDetail().getType().toString());
						}

						if (huEvent.getHandlingUnitDetail().getShipmentUnitType() != null) {
							model.setShipmentUnitType(huEvent.getHandlingUnitDetail().getShipmentUnitType().toString());
						}

						HandlingUnitPhysicalCharacteristic physicalCharacteristics = new HandlingUnitPhysicalCharacteristic();

						if (huEvent.getHandlingUnitDetail().getVolume() != null) {

							Volume volume = new Volume();
							if (huEvent.getHandlingUnitDetail().getVolume().getValue() != null) {
								volume.setValue(huEvent.getHandlingUnitDetail().getVolume().getValue());
							}
							if (huEvent.getHandlingUnitDetail().getVolume().getUnits() != null) {
								volume.setUnits(VolumeUnits
										.fromValue(huEvent.getHandlingUnitDetail().getVolume().getUnits().toString()));
							}

							physicalCharacteristics.setVolume(volume);
						}

						if (huEvent.getHandlingUnitDetail().getWeight() != null) {

							Weight weight = new Weight();
							if (huEvent.getHandlingUnitDetail().getWeight().getValue() != null) {
								weight.setValue(huEvent.getHandlingUnitDetail().getWeight().getValue());
							}
							if ((huEvent.getHandlingUnitDetail().getWeight().getUnits() != null)) {
								weight.setUnits(WeightUnits
										.fromValue(huEvent.getHandlingUnitDetail().getWeight().getUnits().toString()));
							}

							physicalCharacteristics.setWeight(weight);
						}

						if (huEvent.getHandlingUnitDetail().getDimensions() != null) {

							Dimensions dimensions = new Dimensions();
							if (huEvent.getHandlingUnitDetail().getDimensions().getHeight() != null) {
								dimensions.setHeight(huEvent.getHandlingUnitDetail().getDimensions().getHeight());
							}
							if (huEvent.getHandlingUnitDetail().getDimensions().getLength() != null) {
								dimensions.setLength(huEvent.getHandlingUnitDetail().getDimensions().getLength());
							}
							if (huEvent.getHandlingUnitDetail().getDimensions().getUnits() != null) {
								dimensions.setUnits(LinearUnits.fromValue(
										huEvent.getHandlingUnitDetail().getDimensions().getUnits().toString()));
							}
							if (huEvent.getHandlingUnitDetail().getDimensions().getWidth() != null) {
								dimensions.setWidth(huEvent.getHandlingUnitDetail().getDimensions().getWidth());
							}

							physicalCharacteristics.setDimensions(dimensions);
						}

						model.setPhysicalCharacteristic(physicalCharacteristics);

						if (huEvent.getHandlingUnitDetail().getCommodityDetails() != null
								&& !(huEvent.getHandlingUnitDetail().getCommodityDetails().isEmpty())) {
							List<ShipmentCommodityDetail> listCommodityDetail = new ArrayList<ShipmentCommodityDetail>();
							huEvent.getHandlingUnitDetail().getCommodityDetails().forEach(commodity -> {
								ShipmentCommodityDetail commodityDetail = new ShipmentCommodityDetail();
								Money money = new Money();
								if (commodity.getCommodityId() != null) {
									commodityDetail.setCommodityId(commodity.getCommodityId());
								}

								if (null != commodity.getDangerousGoodsInfo()
										&& commodity.getDangerousGoodsInfo().getDgManifestId() != null) {
									model.setDgManifestId(commodity.getDangerousGoodsInfo().getDgManifestId());
								}
								if (commodity.getCommodityType() != null) {
									commodityDetail.setCommodityType(commodity.getCommodityType());
								}
								if (commodity.getCustomsValue() != null) {
									if (commodity.getCustomsValue().getAmount() != null) {
										money.setAmount(commodity.getCustomsValue().getAmount());
									}
									if (commodity.getCustomsValue().getCurrencyCode() != null) {
										money.setCurrencyCode(commodity.getCustomsValue().getCurrencyCode());
									}
									if (commodity.getCustomsValue().getCurrencyType() != null) {
										money.setCurrencyType(commodity.getCustomsValue().getCurrencyType());
									}
									commodityDetail.setCustomsValue(money);
								}

								listCommodityDetail.add(commodityDetail);
							});
//						handlingUnit/handlingUnitDetail/commodityDetails/HandlingUnitcommodityDetail/dangerousGoodInfo/dgManifestId
							model.setCommodity(listCommodityDetail);
						}

						if (huEvent.getHandlingUnitDetail().getRequirements() != null
								&& !(huEvent.getHandlingUnitDetail().getRequirements().isEmpty())) {
							List<WorkRequirement> workRequirementDetailList = new ArrayList<WorkRequirement>();
							huEvent.getHandlingUnitDetail().getRequirements().forEach(element -> {

								List<WorkAttribute> workAttributeList = new ArrayList<WorkAttribute>();
								List<WorkAttribute> additionalWorkAttributeList = new ArrayList<WorkAttribute>();
								List<WorkRequirementStatus> statusList = new ArrayList<WorkRequirementStatus>();
								// List<WorkRequirementStatus> workRequirementStatusList = new
								// ArrayList<WorkRequirementStatus>();
								WorkRequirement workRequirement = new WorkRequirement();
								WorkSource workSource = new WorkSource();
								List<WorkRequirementAssociation> workRequirementAssociationList = new ArrayList<WorkRequirementAssociation>();
								if (element.getAssociations() != null && !element.getAssociations().isEmpty()) {

									element.getAssociations().forEach(associations -> {
										WorkRequirementAssociation association = new WorkRequirementAssociation();
										if (associations.getAssociationTarget() != null) {
											association.setAssociationTarget(
													associations.getAssociationTarget().toString());
										}
										if (associations.getAssociationUUID() != null) {
											association.setAssociationUUID(associations.getAssociationUUID());
										}
										if (associations.getEffectiveDateTime() != null) {
											association.setEffectiveDateTime(
													Date.from(associations.getEffectiveDateTime().toInstant()));
										}
										if (associations.getExpirationDateTime() != null) {
											association.setExpirationDateTime(
													Date.from(associations.getExpirationDateTime().toInstant()));
										}
										workRequirementAssociationList.add(association);
									});
								}
								workRequirement.setAssociations(workRequirementAssociationList);

								if (element.getStatuses() != null && !element.getStatuses().isEmpty()) {
									element.getStatuses().forEach(wReqStatus -> {
										WorkRequirementStatus workRequirementStatus = new WorkRequirementStatus();
										if (wReqStatus.getType() != null) {
											workRequirementStatus.setType(wReqStatus.getType().toString());
										}
										if (wReqStatus.getValue() != null) {
											workRequirementStatus.setValue(wReqStatus.getValue().toString());
										}
										if (wReqStatus.getEffectiveDateTime() != null) {
											workRequirementStatus.setEffectiveDateTime(
													Date.from(wReqStatus.getEffectiveDateTime().toInstant()));
										}
										statusList.add(workRequirementStatus);
									});
								}
								if (element.getSource() != null && element.getSource().getAppId() != null) {
									workSource.setAppId(element.getSource().getAppId());
								}
								if (element.getId() != null) {
									workRequirement.setId(element.getId());
								}
								if (element.getType() != null) {
									workRequirement.setType(element.getType());
								}
								if (element.getMajorVersion() != null) {
									workRequirement.setMajorVersion(element.getMajorVersion());
								}
								if (element.getPriority() != null) {
									workRequirement.setPriority(element.getPriority());
								}
								workRequirement.setSource(workSource);

								if (element.getAdditionalAttributes() != null
										&& !element.getAdditionalAttributes().isEmpty()) {
									element.getAdditionalAttributes().forEach(watt -> {
										WorkAttribute additionalWorkAttribute = new WorkAttribute();
										if (watt.getKey() != null) {
											additionalWorkAttribute.setKey(watt.getKey());
										}
										if (watt.getValues() != null) {
											additionalWorkAttribute.setValues(watt.getValues());
										}
										additionalWorkAttributeList.add(additionalWorkAttribute);
									});
								}

								workRequirement.setAdditionalAttributes(additionalWorkAttributeList);

								if (element.getAttributes() != null && !element.getAttributes().isEmpty()) {
									element.getAttributes().forEach(watt -> {
										WorkAttribute workAttribute = new WorkAttribute();
										if (watt.getKey() != null) {
											workAttribute.setKey(watt.getKey());
										}
										if (watt.getValues() != null) {
											workAttribute.setValues(watt.getValues());
										}
										workAttributeList.add(workAttribute);
									});
								}
								workRequirement.setAttributes(workAttributeList);
								workRequirement.setStatuses(statusList);
								workRequirementDetailList.add(workRequirement);
								if (element.getType().equals("transport-to-location")) {
									if (element.getAttributes() != null && !element.getAttributes().isEmpty()) {
										element.getAttributes().forEach(attribute -> {
											if (attribute.getKey() != null && attribute.getKey().equals("service-code")) {
												model.setLegacyServiceCode(attribute.getValues().toString());
											}
											if (attribute.getKey() != null
													&& attribute.getKey().equalsIgnoreCase("maximum-date-time")) {
												try {
													model.setCommitDate(new SimpleDateFormat("yyyy-MM-ddTHH:mm:ssz")
															.parse(attribute.getValues().get(0)));
												} catch (ParseException e) {
													e.printStackTrace();
													model.setCommitDate(null);
												}
											}
										});
									}
								}
							});
							model.setWorkRequirements(workRequirementDetailList);
						}

						if (huEvent.getHandlingUnitDetail().getConsolidationType() != null) {
							model.setConsolidationType(
									huEvent.getHandlingUnitDetail().getConsolidationType().toString());
						}
						if (huEvent.getHandlingUnitDetail().getUrsaRoutingCode() != null) {
							model.setUrsaSuffix(huEvent.getHandlingUnitDetail().getUrsaRoutingCode());
						}
						if (huEvent.getHandlingUnitDetail().getConsolidationCategory() != null) {
							model.setConsolidationCategory(
									huEvent.getHandlingUnitDetail().getConsolidationCategory().toString());
						}

						if (huEvent.getHandlingUnitDetail().getTypeOfHandlingUnit() != null
								&& !(huEvent.getHandlingUnitDetail().getTypeOfHandlingUnit().isEmpty())) {
//						model.setTypeOfHandlingUnit(huEvent.getHandlingUnitDetail().getTypeOfHandlingUnit());
							model.setTypeOfHandlingUnit(null); // as per koushalya's excel sheet
						}

						if (huEvent.getHandlingUnitDetail().getSpecialHandlingCodes() != null
								&& !(huEvent.getHandlingUnitDetail().getSpecialHandlingCodes().isEmpty())) {
							// Pull necessary fields from CORE SpecialHandlingCodes list
							List<String> specialCodes = new ArrayList<String>();
							for (DescriptionList code : huEvent.getHandlingUnitDetail().getSpecialHandlingCodes()) {
								if (code.getId() != null) {
									specialCodes.add(code.getCode());
								}
							}
							if (specialCodes.size() > 0)
								model.setSpecialHandlingCodes(specialCodes);
						}
					}
					if (huEvent.getIdentificationProfile() != null) {

						if (huEvent.getIdentificationProfile().getCreateDateTime() != null) {
							model.setCreateDateTime(
									Date.from(huEvent.getIdentificationProfile().getCreateDateTime().toInstant()));
						}

						if (huEvent.getIdentificationProfile().getZipCode() != null) {
							model.setZipCode(huEvent.getIdentificationProfile().getZipCode());
						}

						if (huEvent.getIdentificationProfile().getHandlingUnitIds() != null
								&& !(huEvent.getIdentificationProfile().getHandlingUnitIds().isEmpty())) {
							List<HandlingUnitId> handlingUnitIdList = new ArrayList<HandlingUnitId>();
							huEvent.getIdentificationProfile().getHandlingUnitIds().forEach(huIds -> {
								HandlingUnitId huID = new HandlingUnitId(huIds.getIdType(), huIds.getId(),
										huIds.getEntryType(), huIds.getUniqueIdentifier(),
										huIds.getDefiningAuthorityName());

								handlingUnitIdList.add(huID);
							});
							model.setHandlingUnitIds(handlingUnitIdList);
						}

						if (huEvent.getIdentificationProfile().getNaturalIds() != null
								&& !(huEvent.getIdentificationProfile().getNaturalIds().isEmpty())) {

							List<CompositeNaturalId> compositeNaturalId = new ArrayList<CompositeNaturalId>();
							huEvent.getIdentificationProfile().getNaturalIds().forEach(details -> {

								CompositeNaturalId compositeNId = new CompositeNaturalId();

								List<Parameter> list = new ArrayList<Parameter>();
								if (details.getIds() != null && !details.getIds().isEmpty()) {
									details.getIds().forEach(dts -> {
										Parameter parameter = new Parameter();
										parameter.setKey(dts.getKey());
										parameter.setValue(dts.getValue());
										list.add(parameter);
									});
								}
								compositeNId.setIds(list);

								compositeNaturalId.add(compositeNId);
							});
							model.setNaturalIds(compositeNaturalId);
						}

					}

				}
			}
		} catch (

		Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Mappings({ @Mapping(target = "huUUID", ignore = true),
			@Mapping(target = "commitDate", ignore = true),
			@Mapping(target = "handlingUnitIds", ignore = true),
			@Mapping(target = "physicalCharacteristic", ignore = true), @Mapping(target = "ursaSuffix", ignore = true),
			@Mapping(target = "phuwvReleaseVersion", ignore = true), @Mapping(target = "formID", ignore = true),
			@Mapping(target = "originCountryCode", ignore = true),
			@Mapping(target = "destinationCountryCode", ignore = true),
			@Mapping(target = "legacyServiceCode", ignore = true),
			@Mapping(target = "legacyPackageCode", ignore = true),
			@Mapping(target = "typeOfHandlingUnit", ignore = true), @Mapping(target = "naturalIds", ignore = true),
			@Mapping(target = "shipment", ignore = true), @Mapping(target = "specialHandlingCodes", ignore = true),
			@Mapping(target = "type", ignore = true), @Mapping(target = "shipmentUnitType", ignore = true),
			@Mapping(target = "consolidationType", ignore = true), @Mapping(target = "owningOpCo", ignore = true),
			@Mapping(target = "consolidationCategory", ignore = true),
			@Mapping(target = "createDateTime", ignore = true), @Mapping(target = "intendedShipDate", ignore = true),
			@Mapping(target = "zipCode", ignore = true), @Mapping(target = "dgManifestId", ignore = true),
			@Mapping(target = "tasks", ignore = true), @Mapping(target = "commodity", ignore = true),
			@Mapping(target = "waypoints", ignore = true), @Mapping(target = "workRequirements", ignore = true) })
	com.fedex.phuwv.common.dto.HandlingUnit toModel(HandlingUnitDomainEvent coreHandlingUnit);

}
