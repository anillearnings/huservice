package com.fedex.phuwv.handlingunit.service.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.fedex.phuwv.view.handlingunitentity.entity.HandlingUnitXRefEntity;
import com.fedex.phuwv.view.handlingunitentity.entity.HandlingUnitXRefId;

@Repository
public interface HandlingUnitXRefRepository extends JpaRepository<HandlingUnitXRefEntity, HandlingUnitXRefId> {

	
	public HandlingUnitXRefEntity findTopByHandlingUnitRefId_trackingNbr(String trackingNbr);

	public List<HandlingUnitXRefEntity> findByHandlingUnitRefId_handlingUnitUUID(String uuid);

	public List<HandlingUnitXRefEntity> findFirst50000ByRowPurgeDataLessThanEqual(Date purgeDate);

	public List<HandlingUnitXRefEntity> findFirst50000ByRowPurgeDataIsNull();

}
