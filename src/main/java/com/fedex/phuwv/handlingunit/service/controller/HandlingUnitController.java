package com.fedex.phuwv.handlingunit.service.controller;

import java.io.IOException;
import java.text.ParseException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fedex.phuwv.common.dto.HandlingUnit;
import com.fedex.phuwv.common.http.rest.ApiError;
import com.fedex.phuwv.common.logging.Audit;
import com.fedex.phuwv.common.logging.AuditKey;
import com.fedex.phuwv.common.types.StatusTypes;
import com.fedex.phuwv.handlingunit.service.service.HandlingUnitService;
import com.fedex.sefs.core.handlingunit.v1.api.StatusType;
import org.springframework.web.bind.annotation.CrossOrigin;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@CrossOrigin
@RestController
@RequestMapping("/api/v1/phuwv")
public class HandlingUnitController {
	@Autowired
	private HandlingUnitService service;

	@ApiOperation(value = "Get latest handling unit by handling unit UUID", response = HandlingUnit.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "OK", response = HandlingUnit.class),
			@ApiResponse(code = 204, message = "Not found", response = ApiError.class),
			@ApiResponse(code = 400, message = "Bad Request", response = ApiError.class),
			@ApiResponse(code = 500, message = "Internal Server Error", response = ApiError.class),
			@ApiResponse(code = 503, message = "Service Unavailable", response = ApiError.class) })
	@GetMapping(value = "/handlingUnits/{handlingUnitUUID}", produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<HandlingUnit> getLatestHandlingUnitByUUID(
			@PathVariable(value = "handlingUnitUUID") String handlingUnitUUID) throws IOException {

		Audit.getInstance().put(AuditKey.ACTION, "HandlingUnitController.getLatestHandlingUnitByUUID");
		Audit.getInstance().put(AuditKey.IN, handlingUnitUUID);

		HandlingUnit handlingUnitModel = service.getLatestHandlingUnitByUUID(handlingUnitUUID);
		Audit.getInstance().put(AuditKey.STATUS, StatusType.SUCCESS.name());
		Audit.getInstance().put(AuditKey.OUT, "***HandlingUnit Details by handlingUnitUUID***  " + handlingUnitModel);
		return ResponseEntity.ok(handlingUnitModel);

	}

	@ApiOperation(value = "Get list of handling unit by shipmentUUID", response = HandlingUnit.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "OK", response = HandlingUnit.class),
			@ApiResponse(code = 204, message = "No Content"),
			@ApiResponse(code = 400, message = "Bad Request", response = ApiError.class),
			@ApiResponse(code = 500, message = "Internal Server Error", response = ApiError.class),
			@ApiResponse(code = 503, message = "Service Unavailable", response = ApiError.class) })
	@GetMapping(value = "/handlingUnits/getLatestHandlingUnit/{shipmentUUID}", produces = {
			MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<HandlingUnit> getLatestHandlingUnitByShipmentUUID(
			@PathVariable(value = "shipmentUUID") String shipmentUUID) throws IOException {

		Audit.getInstance().put(AuditKey.ACTION, "HandlingUnitController.getLatestHandlingUnitByShipmentUUID");
		Audit.getInstance().put(AuditKey.IN, shipmentUUID);

		HandlingUnit handlingUnitModel = service.getLatestHandlingUnitByShipmentUUID(shipmentUUID);
		Audit.getInstance().put(AuditKey.STATUS, StatusTypes.SUCCESS.name());
		Audit.getInstance().put(AuditKey.OUT, "***HandlingUnit Details by shipmentUUID***  " + handlingUnitModel);

		return ResponseEntity.ok(handlingUnitModel);

	}

	@ApiOperation(value = "Get list of handling unit by shipmentUUID", response = HandlingUnit.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "OK", response = HandlingUnit.class),
			@ApiResponse(code = 204, message = "No Content"),
			@ApiResponse(code = 400, message = "Bad Request", response = ApiError.class),
			@ApiResponse(code = 500, message = "Internal Server Error", response = ApiError.class),
			@ApiResponse(code = 503, message = "Service Unavailable", response = ApiError.class) })
	@GetMapping(value = "/handlingUnits/byShipmentUUID/{shipmentUUID}", produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<List<HandlingUnit>> getListOfLatestHandlingUnitByShipmentUUID(
			@PathVariable(value = "shipmentUUID") String shipmentUUID) throws IOException {

		Audit.getInstance().put(AuditKey.ACTION, "HandlingUnitController.getListOfLatestHandlingUnitByShipmentUUID");
		Audit.getInstance().put(AuditKey.IN, shipmentUUID);
		List<HandlingUnit> handlingUnitModel = service.getListOfLatestHandlingUnitByShipmentUUID(shipmentUUID);
		Audit.getInstance().put(AuditKey.STATUS, StatusTypes.SUCCESS.name());
		Audit.getInstance().put(AuditKey.OUT,
				"***** List of HandlingUnit detials by shipmentUUID ****** " + handlingUnitModel);
		return ResponseEntity.ok(handlingUnitModel);

	}

	@ApiOperation(value = "Get latest handling unit by track number", response = HandlingUnit.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "OK", response = HandlingUnit.class),
			@ApiResponse(code = 204, message = "No Content"),
			@ApiResponse(code = 400, message = "Bad Request", response = ApiError.class),
			@ApiResponse(code = 500, message = "Internal Server Error", response = ApiError.class),
			@ApiResponse(code = 503, message = "Service Unavailable", response = ApiError.class) })
	@GetMapping(value = "/handlingUnits", produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<HandlingUnit> getLatestHandlingUnitByTrackNbr(
			@RequestParam("trkNbr") String handlingUnitTrackNbr)
			throws  IOException {

		Audit.getInstance().put(AuditKey.ACTION, "HandlingUnitController.getLatestHandlingUnitByTrackNbr");
		Audit.getInstance().put(AuditKey.IN, handlingUnitTrackNbr);

		HandlingUnit handlingUnitModel = service.getLatestHandlingUnitByTrackNbr(handlingUnitTrackNbr);
		Audit.getInstance().put(AuditKey.STATUS, StatusTypes.SUCCESS.name());
		Audit.getInstance().put(AuditKey.OUT,
				"***** HandlingUnit detials  by handlingUnitTrackNbr ****** " + handlingUnitModel);

		return ResponseEntity.ok(handlingUnitModel);

	}

	@ApiOperation(value = "Purge handling unit rows by row purge date", response = Integer.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "OK", response = Integer.class),
			@ApiResponse(code = 202, message = "Accept", response = Integer.class),
			@ApiResponse(code = 204, message = "No Content", response = Integer.class),
			@ApiResponse(code = 404, message = "Not found", response = Integer.class) })
	@DeleteMapping(value = "/purgeUntil/{purgeDt}", produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<Integer> purgeUntilDate(@PathVariable(value = "purgeDt") String purgeDt)
			throws ParseException {

		Audit.getInstance().put(AuditKey.ACTION, "HandlingUnitController.purgeUntilDate");
		Audit.getInstance().put(AuditKey.IN, purgeDt);

		int noOfpurgeRows = service.purge(purgeDt);

		Audit.getInstance().put(AuditKey.STATUS, StatusType.SUCCESS.name());
		Audit.getInstance().put(AuditKey.OUT, "***HandlingUnit Details which are purged ***  " + noOfpurgeRows);

		return ResponseEntity.ok(noOfpurgeRows);

	}

}
