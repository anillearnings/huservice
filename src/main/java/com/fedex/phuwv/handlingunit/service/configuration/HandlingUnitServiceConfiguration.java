package com.fedex.phuwv.handlingunit.service.configuration;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

@Configuration
@EnableConfigurationProperties(HandlingUnitServiceProperties.class)
//@EnableRetry
//@ComponentScan(basePackageClasses = { com.fedex.phuwv.common.mapper.HandlingUnitMapper.class })
public class HandlingUnitServiceConfiguration {
	
    @Bean(name = "defaultObjectMapper")
    @Qualifier("defaultObjectMapper")
    @Primary
    public ObjectMapper defaultObjectMapper() {
           ObjectMapper objectMapper = new ObjectMapper();
           objectMapper.registerModule(new JavaTimeModule());
           objectMapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
           objectMapper.disable(DeserializationFeature.ADJUST_DATES_TO_CONTEXT_TIME_ZONE);
           objectMapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
           return objectMapper;
    }
    
}
