//package com.fedex.phuwv.handlingunit.service.util;
//import static org.assertj.core.api.Assertions.assertThat;
//
//import org.junit.jupiter.api.Test;
//import org.junit.jupiter.api.extension.ExtendWith;
//import org.mockito.InjectMocks;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.test.context.junit.jupiter.SpringExtension;
// 
//@ExtendWith(SpringExtension.class)
//@SpringBootTest
//public class HandlingUnitServiceConstantsTest {
//	
// @InjectMocks
// private HandlingUnitServiceConstants constants;
//  
//    @Test
//    public void testConstants() {
//    	
//    	assertThat(HandlingUnitServiceConstants.PLUS).isEqualTo("plus");
//    	assertThat(HandlingUnitServiceConstants.ADDITIONAL_ROWS).isEqualTo("Additional Rows");
//    	assertThat(HandlingUnitServiceConstants.CREATING_JSON).isEqualTo("Creating json");
//    	assertThat(HandlingUnitServiceConstants.NO_HU_BATCHES).isEqualTo("Number of batches to be purged");
//    	assertThat(HandlingUnitServiceConstants.PURGED_MSG).isEqualTo("-DIVE- Purged ");
//    }
//    
//}