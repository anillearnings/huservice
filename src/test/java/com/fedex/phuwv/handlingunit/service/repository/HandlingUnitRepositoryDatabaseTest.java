//package com.fedex.phuwv.handlingunit.service.repository;
//
//import static org.assertj.core.api.Assertions.assertThat;
//
//import java.io.IOException;
//import java.nio.charset.Charset;
//import java.nio.file.Files;
//import java.nio.file.Paths;
//import java.time.Instant;
//import java.time.ZonedDateTime;
//import java.time.temporal.ChronoUnit;
//import java.util.ArrayList;
//import java.util.Calendar;
//import java.util.Date;
//import java.util.List;
//import java.util.Optional;
//
//import org.junit.jupiter.api.AfterEach;
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.Test;
//import org.junit.jupiter.api.extension.ExtendWith;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.test.context.ActiveProfiles;
//import org.springframework.test.context.junit.jupiter.SpringExtension;
//
//import com.fasterxml.jackson.databind.ObjectMapper;
//import com.fedex.phuwv.util.BytesUtil;
//import com.fedex.phuwv.util.CompressionUtil;
//import com.fedex.phuwv.util.JsonUtil;
//import com.fedex.phuwv.view.handlingunitentity.entity.HandlingUnitEntity;
//import com.fedex.phuwv.view.handlingunitentity.entity.HandlingUnitId;
//import com.fedex.phuwv.view.handlingunitentity.entity.HandlingUnitXRefEntity;
//import com.fedex.phuwv.view.handlingunitentity.entity.HandlingUnitXRefId;
//import com.fedex.sefs.core.handlingunit.v1.api.HandlingUnit;
//import com.fedex.sefs.core.handlingunit.v1.api.HandlingUnitIdentificationProfile;
//
//@SpringBootTest
//@ExtendWith(SpringExtension.class)
//@ActiveProfiles("L0")
//public class HandlingUnitRepositoryDatabaseTest {
//
//	@Autowired
//	HandlingUnitRepository repository;
//
//	@Autowired
//	HandlingUnitXRefRepository xRefRepository;
//
//	@Autowired
//	ObjectMapper objectMapper;
//
//	private int maxDataImgColumnLength = 2000;
//	private int maxDataImgColumn = 2;
//	private int maxDataRetentionDays = 30;
//	
//	private String huUUID="521d02f3-9a0b-3ffb-9fa5-2a591e245114";
//
//	private HandlingUnit coreHandlingUnit;
//	private List<HandlingUnitEntity> handlingUnitEntityList;
//
//	private HandlingUnitXRefEntity crossRefEntity;
//	private List<HandlingUnitXRefEntity> handlingUnitXRefEntityList;
//	
//	final Logger logger = LoggerFactory.getLogger(HandlingUnitRepositoryDatabaseTest.class);
//
//	@BeforeEach
//	public void setup() throws IOException {
//
//		String content = readFileContent("src/test/resources/hu_sample2.json", Charset.defaultCharset());
//		coreHandlingUnit = objectMapper.readValue(content, HandlingUnit.class);
//
//		HandlingUnitEntity handlingUnitEntity = new HandlingUnitEntity();
//		ZonedDateTime eventCreateDateTime = coreHandlingUnit.getIdentificationProfile().getCreateDateTime();
//		HandlingUnitId huId = new HandlingUnitId();
//		huId.setHandlingUnitUUID(String.valueOf(coreHandlingUnit.getSystemIdentificationProfile().getUuid()));
//		huId.setEventUUID("EventUUID");
//		huId.setRowUpdateTmstp(Instant.now());
//		handlingUnitEntity.setHandlingUnitId(huId);
//		handlingUnitEntity
//				.setEventType(coreHandlingUnit.getIdentificationProfile().getHandlingUnitIds().get(0).getIdType());
//		handlingUnitEntity.setEventCreateTimestamp(eventCreateDateTime);
//		handlingUnitEntity.setRowPurgeDt(getDefaultPurgeDates());
//		String json = JsonUtil.asCompactJson(objectMapper, coreHandlingUnit);
//		convertJsonToRaws(json, handlingUnitEntity);
//		handlingUnitEntityList = new ArrayList<>();
//		handlingUnitEntityList.add(handlingUnitEntity);
//
//		handlingUnitXRefEntityList = new ArrayList<>();
//		com.fedex.sefs.core.handlingunit.v1.api.HandlingUnitId huID = coreHandlingUnit.getIdentificationProfile()
//				.getHandlingUnitIds().get(0);
//		HandlingUnitXRefId key = new HandlingUnitXRefId();
//		crossRefEntity = new HandlingUnitXRefEntity();
//		String trackNbr = huID.getId();
//		String trackNbrType = huID.getIdType();
//		key.setHandlingUnitUUID(String.valueOf(coreHandlingUnit.getSystemIdentificationProfile().getUuid()));
//		key.setTrackingNbr(trackNbr);
//		key.setTrackingNbrType(trackNbrType);
//		crossRefEntity.setHandlingUnitRefId(key);
//		crossRefEntity.setRowPurgeData(getDefaultPurgeDates());
//		handlingUnitXRefEntityList.add(crossRefEntity);
//
//		List<HandlingUnitEntity> handlingUnitEntityData = repository
//				.findByHandlingUnitId_handlingUnitUUID(huUUID);
//		repository.deleteInBatch(handlingUnitEntityData);
//
//		List<HandlingUnitXRefEntity> crossRefEntity = xRefRepository
//				.findByHandlingUnitRefId_handlingUnitUUID(huUUID);
//		xRefRepository.deleteInBatch(crossRefEntity);
//	}
//
//	@AfterEach
//	public void cleanup() {
//		List<HandlingUnitEntity> handlingUnitEntity = repository
//				.findByHandlingUnitId_handlingUnitUUID(huUUID);
//		repository.deleteInBatch(handlingUnitEntity);
//
//		List<HandlingUnitXRefEntity> crossRefEntity = xRefRepository
//				.findByHandlingUnitRefId_handlingUnitUUID(huUUID);
//		xRefRepository.deleteInBatch(crossRefEntity);
//
//	}
//
//	@Test
//	public void testSavingHandlingUnitToDB()  {
//
//		HandlingUnitEntity entity = handlingUnitEntityList.get(0);
//
//		try {
//			repository.save(entity);
//
//			assertThat(repository.findByHandlingUnitId_handlingUnitUUIDOrderByEventCreateTimestampDesc(
//					entity.getHandlingUnitId().getHandlingUnitUUID()).get(0).getHandlingUnitId())
//							.isEqualTo(entity.getHandlingUnitId());
//
//		} catch (Exception e) {
//			logger.error("failed testSavingHandlingUnitToDB" +e);
//		}
//
//	}
//
//	@Test
//	public void testSavingHandlingUnitXRefMock() throws Exception {
//
//		HandlingUnit huId = coreHandlingUnit;
//		HandlingUnitIdentificationProfile huidp = huId.getIdentificationProfile();
//		List<com.fedex.sefs.core.handlingunit.v1.api.HandlingUnitId> huids = huidp.getHandlingUnitIds();
//
//		for (com.fedex.sefs.core.handlingunit.v1.api.HandlingUnitId hu : huids) {
//			HandlingUnitXRefId key = new HandlingUnitXRefId();
//			HandlingUnitXRefEntity xRefEntity = new HandlingUnitXRefEntity();
//
//			String trackingNbr = hu.getId();
//			String trackNbrType = hu.getIdType();
//			key.setHandlingUnitUUID(String.valueOf(coreHandlingUnit.getSystemIdentificationProfile().getUuid()));
//			key.setTrackingNbrType(trackNbrType);
//			key.setTrackingNbr(trackingNbr);
//			xRefEntity.setHandlingUnitRefId(key);
//			xRefEntity.setRowPurgeData(getDefaultPurgeDates());
//			xRefRepository.save(xRefEntity);
//
//			Optional<HandlingUnitXRefEntity> data = xRefRepository.findById(key);
//			HandlingUnitXRefEntity gdd = null;
//			HandlingUnitXRefId handlingUnitRefId=null;
//if(data.isPresent()) {
//			 gdd = data.get();
//			 if(gdd!=null)
//			 handlingUnitRefId = gdd.getHandlingUnitRefId();
//}
//
//			assertThat(handlingUnitRefId).isEqualTo(xRefEntity.getHandlingUnitRefId());
//		}
//	}
//
//	private void convertJsonToRaws(String compactJson, HandlingUnitEntity entity) throws IOException {
//		int maxColumnLength = this.maxDataImgColumnLength;
//		int totalColumnLength = maxDataImgColumn * maxColumnLength;
//		byte[] byteBuff = CompressionUtil.gzip(compactJson);
//
//		List<byte[]> dataImages = BytesUtil.convertJsonByteArray(byteBuff, maxColumnLength, totalColumnLength);
//		int dataImageToSet = dataImages.size();
//		entity.setDataImg1(dataImages.get(0));
//		if (dataImageToSet > 1) {
//			entity.setDataImg2(dataImages.get(1));
//		}
//	}
//
//	private Date getDefaultPurgeDates() {
//		Instant now = Instant.now();
//		Instant purgeInstant = now.plus(maxDataRetentionDays, ChronoUnit.DAYS);
//		Date date = Date.from(purgeInstant);
//		Calendar calendar = Calendar.getInstance();
//		calendar.setTime(date);
//		calendar.set(Calendar.HOUR_OF_DAY, 0);
//		calendar.set(Calendar.MINUTE, 0);
//		calendar.set(Calendar.SECOND, 0);
//		calendar.set(Calendar.MILLISECOND, 0);
//		return calendar.getTime();
//	}
//
//	static String readFileContent(String path, Charset encoding) throws IOException {
//		byte[] encoded = Files.readAllBytes(Paths.get(path));
//		return new String(encoded, encoding);
//	}
//
//}
