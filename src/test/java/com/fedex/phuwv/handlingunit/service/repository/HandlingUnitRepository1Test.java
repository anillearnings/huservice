//package com.fedex.phuwv.handlingunit.service.repository;
//
//import static org.assertj.core.api.Assertions.assertThat;
//
//import java.io.IOException;
//import java.nio.charset.Charset;
//import java.nio.file.Files;
//import java.nio.file.Paths;
//import java.time.Instant;
//import java.time.ZonedDateTime;
//import java.time.temporal.ChronoUnit;
//import java.util.ArrayList;
//import java.util.Calendar;
//import java.util.Date;
//import java.util.List;
//
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.Test;
//import org.junit.jupiter.api.extension.ExtendWith;
//import org.mockito.Mockito;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.boot.test.mock.mockito.MockBean;
//import org.springframework.test.context.junit.jupiter.SpringExtension;
//import org.springframework.test.web.servlet.MockMvc;
//
//import com.fasterxml.jackson.databind.ObjectMapper;
//import com.fedex.phuwv.handlingunit.service.service.HandlingUnitService;
//import com.fedex.phuwv.util.BytesUtil;
//import com.fedex.phuwv.util.CompressionUtil;
//import com.fedex.phuwv.util.JsonUtil;
//import com.fedex.phuwv.view.handlingunitentity.entity.HandlingUnitEntity;
//import com.fedex.phuwv.view.handlingunitentity.entity.HandlingUnitId;
//import com.fedex.sefs.core.handlingunit.v1.api.HandlingUnit;
//
//import lombok.extern.slf4j.Slf4j;
//
//@SpringBootTest
//@ExtendWith(SpringExtension.class)
//@AutoConfigureMockMvc
//@Slf4j
//public class HandlingUnitRepository1Test {
//
//	@Autowired
//	MockMvc mockMvc;
//
//	@MockBean
//	HandlingUnitService service;
//
//	@MockBean
//	HandlingUnitRepository repository;
//
//	@Autowired
//	ObjectMapper objectMapper;
//
//	private int maximumDataImgColumnLength = 2000;
//	private int maximumDataImgColumn = 2;
//	private int maximumDataRetentionDays = 30;
//
//	private HandlingUnit coreHandlingUnit;
//	private List<HandlingUnitEntity> huEntityList;
//
//	@BeforeEach
//	public void setup() throws IOException {
//
//		String content = readFile("src/test/resources/hu_sample2.json", Charset.defaultCharset());
//		coreHandlingUnit = objectMapper.readValue(content, HandlingUnit.class);
//
//		HandlingUnitEntity entity = new HandlingUnitEntity();
//		HandlingUnitId handlingUnitId;
//		ZonedDateTime eventCreateDateTime = coreHandlingUnit.getIdentificationProfile().getCreateDateTime();
//		handlingUnitId = new HandlingUnitId(coreHandlingUnit.getSystemIdentificationProfile().getUuid().toString(),
//				coreHandlingUnit.getSystemIdentificationProfile().getUuid().toString(), Instant.now());
//		entity.setHandlingUnitId(handlingUnitId);
//		entity.setEventType(coreHandlingUnit.getIdentificationProfile().getHandlingUnitIds().get(0).getIdType());
//		entity.setEventCreateTimestamp(eventCreateDateTime);
//		entity.setRowPurgeDt(getDefaultPurgeDate());
//		String json = JsonUtil.asCompactJson(objectMapper, coreHandlingUnit);
//		convertJsonToRaw(json, entity);
//
//		huEntityList = new ArrayList<>();
//		huEntityList.add(entity);
//		Mockito.when(repository.findByHandlingUnitId_handlingUnitUUIDOrderByEventCreateTimestampDesc("597783866512"))
//				.thenReturn(huEntityList);
//	}
//
//	@Test
//	public void testSavingHandlingUnitMockReturn() {
//
//		try {
//			assertThat(repository.findByHandlingUnitId_handlingUnitUUIDOrderByEventCreateTimestampDesc(
//					coreHandlingUnit.getIdentificationProfile().getHandlingUnitIds().get(0).getId()))
//							.isEqualTo(huEntityList);
//		} catch (Exception e) {
//			log.info("exception while Saving HandlingUnit Mock");
//		}
//
//	}
//
//	private void convertJsonToRaw(String compactJson, HandlingUnitEntity entity) throws IOException {
//		int maximumColumnLength = this.maximumDataImgColumnLength;
//		int totalColumnLength = maximumDataImgColumn * maximumColumnLength;
//		byte[] byteBuff = CompressionUtil.gzip(compactJson);
//
//		List<byte[]> dataImages = BytesUtil.convertJsonByteArray(byteBuff, maximumColumnLength, totalColumnLength);
//		int dataImageToSet = dataImages.size();
//		entity.setDataImg1(dataImages.get(0));
//		if (dataImageToSet > 1) {
//			entity.setDataImg2(dataImages.get(1));
//		}
//	}
//
//	private Date getDefaultPurgeDate() {
//		Instant now = Instant.now();
//		Instant purgeInstant = now.plus(maximumDataRetentionDays, ChronoUnit.DAYS);
//		Date date = Date.from(purgeInstant);
//		Calendar cal = Calendar.getInstance();
//		cal.setTime(date);
//		cal.set(Calendar.HOUR_OF_DAY, 0);
//		cal.set(Calendar.MINUTE, 0);
//		cal.set(Calendar.SECOND, 0);
//		cal.set(Calendar.MILLISECOND, 0);
//		return cal.getTime();
//	}
//
//	static String readFile(String path, Charset encoding) throws IOException {
//		byte[] encoded = Files.readAllBytes(Paths.get(path));
//		return new String(encoded, encoding);
//	}
//
//}
