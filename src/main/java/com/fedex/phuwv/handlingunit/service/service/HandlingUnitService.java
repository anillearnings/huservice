package com.fedex.phuwv.handlingunit.service.service;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.NoSuchElementException;

import org.apache.commons.collections4.CollectionUtils;
import org.mapstruct.factory.Mappers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fedex.phuwv.common.dto.HandlingUnit;
import com.fedex.phuwv.common.exception.ApplicationException;
import com.fedex.phuwv.common.mapper.HandlingUnitModelMapper;
import com.fedex.phuwv.handlingunit.service.configuration.HandlingUnitServiceProperties;
import com.fedex.phuwv.handlingunit.service.repository.HandlingUnitRepository;
import com.fedex.phuwv.handlingunit.service.repository.HandlingUnitXRefRepository;
import com.fedex.phuwv.handlingunit.service.util.HandlingUnitServiceConstants;
import com.fedex.phuwv.util.BytesUtil;
import com.fedex.phuwv.view.handlingunitentity.entity.HandlingUnitEntity;
import com.fedex.phuwv.view.handlingunitentity.entity.HandlingUnitXRefEntity;
import com.fedex.sefs.core.handlingunit.v1.api.HandlingUnitDomainEvent;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class HandlingUnitService {

	@Autowired
	HandlingUnitRepository handlingUnitRepository;

	@Autowired
	HandlingUnitXRefRepository handlingUnitXRefRepository;

	@Autowired
	ObjectMapper defaultObjectMapper;

	@Autowired
	HandlingUnitServiceProperties properties;

	private HandlingUnitModelMapper modelMapstructMapper = Mappers.getMapper(HandlingUnitModelMapper.class);

	public HandlingUnit getLatestHandlingUnitByTrackNbr(String handlingUnitTrackNbr) throws IOException {

		 HandlingUnitXRefEntity entityList = handlingUnitXRefRepository
					.findTopByHandlingUnitRefId_trackingNbr(handlingUnitTrackNbr);

			if (entityList==null) {
				log.info("No handlingUnitXRef entities found by searching on tracking number");
				throw new NoSuchElementException("No handlingUnit found by trackingNbr:" + handlingUnitTrackNbr);
			}


		return this.getLatestHandlingUnitByUUID(entityList.getHandlingUnitRefId().getHandlingUnitUUID());

	}

	public HandlingUnit getLatestHandlingUnitByUUID(String handlingUnitUUID) throws IOException {

		log.info("-DIVE- Calling getHandlingUnitByHandlingUnitUUID UUID: " + handlingUnitUUID);

		List<HandlingUnitEntity> entityList = handlingUnitRepository
				.findByHandlingUnitId_handlingUnitUUIDOrderByEventCreateTimestampDesc(handlingUnitUUID);

		if (CollectionUtils.isEmpty(entityList)) {
			log.info("No handlingUnit found by uuid");
			throw new NoSuchElementException(HandlingUnitServiceConstants.HANDLINGUNIT_NOT_FOUND + handlingUnitUUID);
		}

		return getHUModel(entityList);
	}

	public HandlingUnit getHUModel(List<HandlingUnitEntity> entityList) throws IOException {
		log.debug("Getting first in list of size: " + entityList.size());
		HandlingUnitEntity entity = entityList.get(0);

		log.debug(HandlingUnitServiceConstants.CREATING_JSON);
		String json = getJsonFromDataImg(entity);
		log.debug("Got json from entity");
		log.debug(json);

		HandlingUnitDomainEvent coreHandlingUnit = defaultObjectMapper.readValue(json, HandlingUnitDomainEvent.class);
		HandlingUnit model = null;
		try {
			model = modelMapstructMapper.toModel(coreHandlingUnit);
		} catch (Exception e1) {
			log.error("Exception while processing handlingunit" + e1.getMessage());
			throw new ApplicationException("" + e1);
		}
		return model;
	}

	public HandlingUnit getLatestHandlingUnitByShipmentUUID(String shipmentUUID) throws IOException {
		log.debug("******* Inside getLatestHandlingUnitByShipmentUUID");
		log.info("-DIVE- Calling getLatestHandlingUnitByShipmentUUID UUID: " + shipmentUUID);

		List<HandlingUnitEntity> entityList = handlingUnitRepository
				.findByShipmentUUIDOrderByEventCreateTimestampDesc(shipmentUUID);

		if (CollectionUtils.isEmpty(entityList)) {
			log.info("No handlingUnit found by shipmentuuid");
			throw new NoSuchElementException(HandlingUnitServiceConstants.HANDLINGUNIT_NOT_FOUND + shipmentUUID);
		}
		return getHUModel(entityList);
	}

	public List<HandlingUnit> getListOfLatestHandlingUnitByShipmentUUID(String shipmentUUID) throws IOException {
		log.debug("******* Inside getHandlingUnitByHandlingUnitUUID");
		log.info("-DIVE- Calling getListOfLatestHandlingUnitByShipmentUUID UUID: " + shipmentUUID);

		List<HandlingUnitEntity> entityList = handlingUnitRepository
				.findByShipmentUUIDOrderByEventCreateTimestampDesc(shipmentUUID);

		if (CollectionUtils.isEmpty(entityList)) {
			log.info("No handlingUnit found by shipmentuuid");
			throw new NoSuchElementException(HandlingUnitServiceConstants.HANDLINGUNIT_NOT_FOUND + shipmentUUID);
		}

		List<HandlingUnit> handlingUnitModel = new ArrayList<>();
		for (int i = 0; i < entityList.size(); i++) {
			log.debug(HandlingUnitServiceConstants.CREATING_JSON);
			String json = getJsonFromDataImg(entityList.get(i));
			log.debug("Got json from entity ");
			log.debug(json);

			HandlingUnitDomainEvent handlingUnitDomainEvent = defaultObjectMapper.readValue(json,
					HandlingUnitDomainEvent.class);
			handlingUnitModel.add(modelMapstructMapper.toModel(handlingUnitDomainEvent));
		}
		return handlingUnitModel;

	}

	

	public int purge(String purgeDtStr) throws ParseException {
		log.info("-DIVE- Purge for HandlingUnitService starting with Date: " + purgeDtStr);

		Date purgeDt = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").parse(purgeDtStr + " 00:00:00");
		int totalCount = 0;

		boolean purgeDaily = properties.isHuPurgeDaily();
		if (purgeDaily) {
			totalCount = totalCount + purgeHandlingUnit(purgeDt);
			totalCount = totalCount + purgeHandlingUnitXRef(purgeDt);
		}

		boolean purgeNull = properties.isHuPurgeNull();
		if (purgeNull) {
			totalCount = totalCount + purgeNullHandlingUnit();
			totalCount = totalCount + purgeNullHandlingUnitXRef();
		}
		log.info("-DIVE- Purge for HandlingUnitService Complete");

		return totalCount;
	}

	private int purgeHandlingUnit(Date purgeDt) {

		

		log.info("-DIVE- Calling HandlingUnitService.purgeHandlingUnit Date: " + purgeDt);

		List<HandlingUnitEntity> entitiesToPurge = handlingUnitRepository
				.findFirst50000ByRowPurgeDtLessThanEqual(purgeDt);

		return getPurgeRowsNumber(entitiesToPurge);
	}

	private int getPurgeRowsNumber( List<HandlingUnitEntity> entitiesToPurge) {
		int purgeRows = entitiesToPurge.size();
		List<HandlingUnitEntity> batchList;
		log.info(HandlingUnitServiceConstants.RETRIEVED_MSG + purgeRows
				+ " handling unit entities eligible to be purged");
		int nbrBatches = 0;
		int batchSize = properties.getHuBatchSize();

		log.debug("hu batchSize = " + batchSize);
		if (0 != batchSize) {
			nbrBatches = entitiesToPurge.size() / batchSize;
		}

		int leftoverEntities = entitiesToPurge.size() - (batchSize * nbrBatches);
		log.info(HandlingUnitServiceConstants.NO_HU_BATCHES + nbrBatches + HandlingUnitServiceConstants.PLUS
				+ leftoverEntities + HandlingUnitServiceConstants.ADDITIONAL_ROWS);

		for (int batches = 0; batches < nbrBatches; batches++) {
			log.debug("inside for loop, iteration #" + batches);
			log.debug("entitiesToPurge list size = " + entitiesToPurge.size());
			batchList = entitiesToPurge.subList(batches * batchSize, (batches + 1) * batchSize);
			log.debug("batchList size = " + batchList.size());
			try {
				if(!batchList.isEmpty()) {
				handlingUnitRepository.deleteInBatch(batchList);
				}
				log.debug("deleted Batch");
			} catch (Exception e) {
				log.info(HandlingUnitServiceConstants.EXCEPTION_MSG, e);
				purgeRows = purgeRows - batchSize;
			}
		}

		log.debug("get  leftover batch subList");
		log.debug("subList start position: " + nbrBatches * batchSize);
		log.debug("sublist end position: " + (entitiesToPurge.size()));
		batchList = entitiesToPurge.subList(nbrBatches * batchSize, entitiesToPurge.size());
		log.debug("Leftover batch size = " + batchList.size());
		try {
			if(!batchList.isEmpty()) {
			handlingUnitRepository.deleteInBatch(batchList);}
			log.debug("deleted leftover batch");
		} catch (Exception e) {
			log.info(HandlingUnitServiceConstants.EXCEPTION_MSG, e);
			purgeRows = purgeRows - leftoverEntities;
		}

		log.info(HandlingUnitServiceConstants.PURGED_MSG + purgeRows + " handling unit rows total.");
		return purgeRows;
	}

	private int purgeNullHandlingUnit() {

		log.info("-DIVE- Calling HandlingUnitService.purgeNullHandlingUnit");
		List<HandlingUnitEntity> entitiesToPurge = handlingUnitRepository.findFirst50000ByRowPurgeDtIsNull();
		
		return getPurgeRowsNumber( entitiesToPurge);
	}

	private int purgeHandlingUnitXRef(Date purgeDt) {

		int nbrBatches = 0;

		log.info("-DIVE- Calling HandlingUnitService.purgeHandlingUnitXRef Date: " + purgeDt);
		List<HandlingUnitXRefEntity> entitiesToPurge = handlingUnitXRefRepository
				.findFirst50000ByRowPurgeDataLessThanEqual(purgeDt);
		

		int batchSize = properties.getHuXrefBatchSize();
		log.debug("huXref batchSize = " + batchSize);
		if (batchSize != 0) {
			nbrBatches = entitiesToPurge.size() / batchSize;
		}
		int leftoverEntities = entitiesToPurge.size() - (batchSize * nbrBatches);
		log.info("nbr handling unit xref batches to purge: " + nbrBatches + " plus " + leftoverEntities
				+ " additional rows");

		return getPurgeRowsForHURef(nbrBatches, entitiesToPurge, batchSize, leftoverEntities);
	}

	private int purgeNullHandlingUnitXRef() {

		int nbrBatches = 0;

		log.info("-DIVE- Calling HandlingUnitService.purgeNullHandlingUnitXRef");
		List<HandlingUnitXRefEntity> entitiesToPurge = handlingUnitXRefRepository.findFirst50000ByRowPurgeDataIsNull();
		int purgeRows = entitiesToPurge.size();
		log.info(HandlingUnitServiceConstants.RETRIEVED_MSG + purgeRows
				+ " NULL handling unit xref entities eligible to be purged");

		int batchSize = properties.getHuXrefBatchSize();
		log.debug("huXref batchSize = " + batchSize);
		if (batchSize != 0)
			nbrBatches = entitiesToPurge.size() / batchSize;
		int leftoverEntities = entitiesToPurge.size() - (batchSize * nbrBatches);
		log.info(HandlingUnitServiceConstants.NO_HU_BATCHES + nbrBatches + HandlingUnitServiceConstants.PLUS
				+ leftoverEntities + HandlingUnitServiceConstants.ADDITIONAL_ROWS);

		return getPurgeRowsForHURef(nbrBatches, entitiesToPurge, batchSize, leftoverEntities);
	}

	public int getPurgeRowsForHURef(int nbrBatches, List<HandlingUnitXRefEntity> entitiesToPurge, 
			int batchSize, int leftoverEntities) {
		List<HandlingUnitXRefEntity> batchList;
		int purgeRows = entitiesToPurge.size();
		for (int batches = 0; batches < nbrBatches; batches++) {
			batchList = entitiesToPurge.subList(batches * batchSize, (batches + 1) * batchSize);
			try {
				if(!batchList.isEmpty()) {
				handlingUnitXRefRepository.deleteInBatch(batchList);}
			} catch (Exception e) {
				log.info("Caught Exception trying to delete handling unit xef Batch ", e);
				purgeRows = purgeRows - batchSize;
			}
		}

		batchList = entitiesToPurge.subList(nbrBatches * batchSize, entitiesToPurge.size());
		try {
			if (!batchList.isEmpty()) {
				handlingUnitXRefRepository.deleteInBatch(batchList);
			}
		} catch (Exception e) {
			log.info("Caught Exception trying to delete handling unit xref Batch ", e);
			purgeRows = purgeRows - leftoverEntities;
		}

		log.info(HandlingUnitServiceConstants.PURGED_MSG + purgeRows + " handling unit xref rows total.");
		return purgeRows;
	}

	

	public String getJsonFromDataImg(HandlingUnitEntity entity) throws IOException {
		// Update as data columns are added
		String retString = null;
		byte[] dataImage1 = entity.getDataImg1();
		byte[] dataImage2 = entity.getDataImg2();
		if ((null != dataImage1) || (null != dataImage2)) {
			return BytesUtil.convertDataImageToJson(dataImage1, dataImage2);
		}

		return retString;
	}

}
